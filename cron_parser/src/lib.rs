#[cfg(test)]
mod tests;

//using some incredibly weird types here to avoid allocating String's
const MONTHS: &[[char; 3]] = &[
	['J', 'A', 'N'],
	['F', 'E', 'B'],
	['M', 'A', 'R'],
	['A', 'P', 'R'],
	['M', 'A', 'Y'],
	['J', 'U', 'N'],
	['J', 'U', 'L'],
	['A', 'U', 'G'],
	['S', 'E', 'P'],
	['O', 'C', 'T'],
	['N', 'O', 'V'],
	['D', 'E', 'C'],
];

const DAYS: &[[char; 3]] = &[
	['S', 'U', 'N'],
	['M', 'O', 'N'],
	['T', 'U', 'E'],
	['W', 'E', 'D'],
	['T', 'H', 'U'],
	['F', 'R', 'I'],
	['S', 'A', 'T'],
];

#[derive(Debug, PartialEq, Eq)]
enum ListElem {
	FullRange(usize),                     // */{NonZero-uint}
	EmptyList,                            // ?
	LastDay,                              // L
	LastWeekday,                          // LW
	Num(usize),                           // {uint}
	Range(std::ops::Range<usize>, usize), // {uint}-{uint}/{NonZero-uint}
	InvertedRange(usize, usize, usize),   // {uint}-{uint}/{NonZero-uint}
	LastNth(usize),                       // {uint}L
	NearestWeekday(usize),                // {uint}W
	NthDayOfMth(usize, usize),            // {uint}#{uint}
}

#[derive(Debug, Default)]
struct ListProcessOptions {
	allow_empty: bool,
	allow_last_day: bool,
	allow_last_weekday: bool,
	allow_last_nth: bool,
	allow_nearest_weekday: bool,
	allow_nth_weekday_of_mth: bool,
	has_stable_upper_bound: bool,
}

const NO_EXTRA_OPTS: ListProcessOptions = ListProcessOptions {
	allow_empty: false,
	allow_last_day: false,
	allow_last_weekday: false,
	allow_last_nth: false,
	allow_nearest_weekday: false,
	allow_nth_weekday_of_mth: false,
	has_stable_upper_bound: false,
};

const DAY_OF_MTH_OPTS: ListProcessOptions = ListProcessOptions {
	allow_empty: true,
	allow_last_day: true,
	allow_last_weekday: true,
	allow_last_nth: false,
	allow_nearest_weekday: true,
	allow_nth_weekday_of_mth: false,
	has_stable_upper_bound: false,
};

pub fn print_output(elems: [String; 6]) {
	println!("{:<14}{}", "minute", elems[0]);
	println!("{:<14}{}", "hour", elems[1]);
	println!("{:<14}{}", "day of month", elems[2]);
	println!("{:<14}{}", "month", elems[3]);
	println!("{:<14}{}", "day of week", elems[4]);
	println!("{:<14}{}", "command", elems[5]);
}

pub fn parse_cron_expression(pat: &str) -> Result<[String; 6], String> {
	fn check_empty(txt: &str) -> Result<(), String> {
		if txt.is_empty() {
			Err("Unexpected end of input".to_string())
		} else {
			Ok(())
		}
	}

	// {
	// 	if pat == "@yearly" {}
	// }

	//bounds on const generics are inner-inclusive and upper exclusive
	let (minute_occur, pat) = process_list::<0, 60, _>(pat, NO_EXTRA_OPTS, &extract_uint)?;
	check_empty(pat)?;

	let (hour_occur, pat) = process_list::<0, 24, _>(pat, NO_EXTRA_OPTS, &extract_uint)?;
	check_empty(pat)?;

	let (day_of_mth_occ, pat) = process_list::<1, 32, _>(pat, DAY_OF_MTH_OPTS, &extract_uint)?;
	check_empty(pat)?;
	let day_of_mth_occ_is_empty = day_of_mth_occ == "?";

	let (month_occur, pat) = process_list::<1, 13, _>(pat, NO_EXTRA_OPTS, &|x| {
		extract_uint_with_aliases(x, MONTHS)
	})?;
	check_empty(pat)?;

	let (day_of_week_occur, pat) = process_list::<0, 7, _>(
		pat,
		ListProcessOptions {
			allow_empty: !day_of_mth_occ_is_empty,
			allow_last_day: true,
			allow_last_weekday: true,
			allow_last_nth: true,
			allow_nearest_weekday: false,
			allow_nth_weekday_of_mth: true,
			has_stable_upper_bound: true,
		},
		&|x| {
			if let Ok((res, rest)) = extract_uint(x) {
				if res == 7 {
					return Ok((0, rest));
				}
			}
			extract_uint_with_aliases(x, DAYS)
		},
	)?;

	check_empty(pat)?;

	let pat = pat.trim_start();

	Ok([
		minute_occur,
		hour_occur,
		day_of_mth_occ,
		month_occur,
		day_of_week_occur,
		pat.to_string(),
	])
}

fn occ_to_str(occ: &[usize], append_after_num: Option<&str>) -> String {
	//I could reserve (a pessimistic length upper bound maybe?) here do avoid allocation burden
	let nums: String = occ
		.iter()
		.enumerate()
		.filter(|(_, &num)| num > 0)
		.map(|(idx, _)| {
			let mut ret = idx.to_string();
			if let Some(x) = append_after_num {
				ret.push_str(x);
			}
			ret.push(' ');
			ret
		})
		.collect();
	nums.trim().to_string()
}

fn process_list<'a, const MIN_BOUND: usize, const MAX_BOUND: usize, F>(
	mut pat: &'a str,
	opts: ListProcessOptions,
	extractor: &F,
) -> Result<(String, &'a str), String>
where
	F: Fn(&str) -> Result<(usize, &str), String>,
{
	let mut nth_iter = 0;
	let mut normal_range = [0; MAX_BOUND];
	let mut last_nth_range = [0; MAX_BOUND];
	let mut nearest_to_nth_range = [0; MAX_BOUND];
	let mut nth_x_range = [[0; MAX_BOUND]; 5];
	let mut last_weekday_set = false;
	let mut last_day_set = false;
	loop {
		let (expr, rest) = process_list_elem(pat, extractor)?;
		match expr {
			ListElem::FullRange(step) => {
				(MIN_BOUND..MAX_BOUND)
					.step_by(step)
					.for_each(|x| normal_range[x] += 1);
			}
			ListElem::Num(n) => {
				if (MIN_BOUND..MAX_BOUND).contains(&n) {
					normal_range[n] += 1;
				} else {
					return Err("Numeric list element out of bounds detected".to_string());
				}
			}
			ListElem::Range(range, step) => {
				if range.start >= MIN_BOUND && range.end <= MAX_BOUND {
					range.step_by(step).for_each(|x| normal_range[x] += 1);
				} else {
					return Err("Range list element out of bounds detected".to_string());
				}
			}
			ListElem::InvertedRange(first, second, step) => {
				//TODO - correct bounds check + test on bounds
				if first >= MIN_BOUND && second <= MAX_BOUND {
					(first..MAX_BOUND)
						.step_by(step)
						.for_each(|x| normal_range[x] += 1);
					(MIN_BOUND..=second)
						.step_by(step)
						.for_each(|x| normal_range[x] += 1);
				} else {
					return Err("Range list element out of bounds detected".to_string());
				}
			}
			ListElem::EmptyList => {
				if !opts.allow_empty {
					return Err("Empty list where it is not allowed detected".to_string());
				}
				if nth_iter > 0 {
					return Err("Empty list as non-unique element detected".to_string());
				} else if !(rest.is_empty() || rest.starts_with(char::is_whitespace)) {
					return Err("Empty list with trailing elements detected".to_string());
				} else {
					return Ok(("?".to_string(), rest.trim_start()));
				}
			}
			ListElem::LastDay => {
				if !opts.allow_last_day {
					return Err("'Last Day' where it is not allowed detected".to_string());
				}
				if opts.has_stable_upper_bound {
					normal_range[MAX_BOUND - 1] += 1;
				} else {
					last_day_set = true;
				}
			}
			ListElem::LastWeekday => {
				if !opts.allow_last_weekday {
					return Err("'Last WeekDay' where it is not allowed detected".to_string());
				}
				last_weekday_set = true;
			}
			ListElem::LastNth(n) => {
				if !opts.allow_last_nth {
					return Err("'Last Nth Day' where it is not allowed detected".to_string());
				}
				if n >= MIN_BOUND && n < MAX_BOUND {
					last_nth_range[n] += 1;
				} else {
					return Err("'Last Nth Day' out of bounds detected".to_string());
				}
			}
			ListElem::NearestWeekday(n) => {
				if !opts.allow_nearest_weekday {
					return Err("'Nearest-to Weekday' where it is not allowed detected".to_string());
				}
				if n >= MIN_BOUND && n < MAX_BOUND {
					nearest_to_nth_range[n] += 1;
				} else {
					return Err("'Nearest-to Weekday' out of bounds detected".to_string());
				}
			}
			ListElem::NthDayOfMth(day, n) => {
				if !opts.allow_nth_weekday_of_mth {
					return Err("'Nth N_Day OfMth' where it is not allowed detected".to_string());
				}
				//valid week numbers 1-5
				if (1..=5).contains(&n) {
					nth_x_range[n - 1][day] += 1;
				} else {
					return Err("'Nth N_Day OfMth' week must be between 1 and 5 inclusive".to_string());
				}
			}
		};
		nth_iter += 1;
		if let Some(rest) = rest.strip_prefix(',') {
			pat = rest;
			continue;
		}
		if rest.is_empty() || rest.starts_with(char::is_whitespace) {
			pat = rest.trim_start();
			break;
		}
	}

	//format the display string
	//veeeeery inneficient in the grand scheme of things!
	let normal_range_str = occ_to_str(&normal_range, None);
	let last_nth_range_str = occ_to_str(&last_nth_range, Some("L"));
	let nearest_to_nth_range_str = occ_to_str(&nearest_to_nth_range, Some("W"));
	let nth_x_range_str: String = nth_x_range
		.iter()
		.enumerate()
		.map(|(idx, &x)| {
			let appended = format!("{}{}", "#", idx + 1);
			let mut nth_day_str = occ_to_str(&x, Some(&appended));
			nth_day_str.push(' ');
			nth_day_str
		})
		.collect();
	let nth_x_range_str = nth_x_range_str.trim().to_string();

	let mut output = String::new();
	if !normal_range_str.is_empty() {
		output.push_str(&normal_range_str);
		output.push(' ');
	}
	if !last_nth_range_str.is_empty() {
		output.push_str(&last_nth_range_str);
		output.push(' ');
	}
	if !nearest_to_nth_range_str.is_empty() {
		output.push_str(&nearest_to_nth_range_str);
		output.push(' ');
	}
	if !nth_x_range_str.is_empty() {
		output.push_str(&nth_x_range_str);
		output.push(' ');
	}
	output.push_str(if last_weekday_set { "LW " } else { "" });
	output.push_str(if last_day_set { "L " } else { "" });
	Ok((output.trim().to_string(), pat))
}

fn process_list_elem<'a, F>(pat: &'a str, extractor: &F) -> Result<(ListElem, &'a str), String>
where
	F: Fn(&str) -> Result<(usize, &str), String>,
{
	fn process_step(pat: &str) -> Result<(usize, &str), String> {
		if pat.is_empty() || pat.starts_with(',') || pat.starts_with(char::is_whitespace) {
			Ok((1, pat))
		} else if let Some(pat) = pat.strip_prefix('/') {
			let (step, pat) = extract_uint(pat)?;
			if step == 0 {
				Err("Step of 0 detected".to_string())
			} else {
				Ok((step, pat))
			}
		} else {
			Err("Unexpected token encountered while trying to process step".to_string())
		}
	}

	if pat.is_empty() {
		return Err("Empty List detected while trying to parse list element".to_string());
	}
	if let Some(pat) = pat.strip_prefix('?') {
		return Ok((ListElem::EmptyList, pat));
	}
	if let Some(pat) = pat.strip_prefix("LW") {
		return Ok((ListElem::LastWeekday, pat));
	}
	if let Some(pat) = pat.strip_prefix('L') {
		return Ok((ListElem::LastDay, pat));
	}
	if let Some(pat) = pat.strip_prefix('*') {
		let (step, pat) = process_step(pat)?;
		return Ok((ListElem::FullRange(step), pat));
	}
	let (num, pat) = extractor(pat)?;
	if pat.is_empty() || pat.starts_with(',') || pat.starts_with(char::is_whitespace) {
		return Ok((ListElem::Num(num), pat));
	}
	match pat.chars().next().unwrap() {
		'-' => {
			let (num2, pat) = extractor(&pat[1..])?;
			let (step, pat) = process_step(pat)?;
			if num2 < num {
				Ok((ListElem::InvertedRange(num, num2, step), pat))
			} else {
				Ok((ListElem::Range(num..num2 + 1, step), pat))
			}
		}
		'L' => Ok((ListElem::LastNth(num), &pat[1..])),
		'W' => Ok((ListElem::NearestWeekday(num), &pat[1..])),
		'#' => {
			let (num2, pat) = extractor(&pat[1..])?;
			Ok((ListElem::NthDayOfMth(num, num2), pat))
		}
		_ => Err("Unexpected token detected while trying to parse list element".to_string()),
	}
}

fn extract_uint_with_aliases<'a>(
	expr: &'a str,
	aliases: &[[char; 3]],
) -> Result<(usize, &'a str), String> {
	if expr.len() >= 3 && !expr.chars().next().unwrap().is_ascii_digit() {
		let mut get_nth_or_null = expr.chars().take(3);
		let tmp_str = [
			get_nth_or_null.next().unwrap_or('\0').to_ascii_uppercase(),
			get_nth_or_null.next().unwrap_or('\0').to_ascii_uppercase(),
			get_nth_or_null.next().unwrap_or('\0').to_ascii_uppercase(),
		];
		//instead of linear search i could use a more efficient data structure here for constant lookup
		//but:
		//1) it would interfere with the language semantics... (more allocations + i will lose consts and generics)
		//2) the search space is small and constrained to this module
		if let Some(x) = aliases.iter().position(|x| x == &tmp_str) {
			return Ok((x, &expr[3..]));
		}
	}
	extract_uint(expr)
}

fn extract_uint(expr: &str) -> Result<(usize, &str), String> {
	let pos = expr
		.chars()
		.position(|x| !x.is_ascii_digit())
		.unwrap_or(expr.len());
	let num: Result<usize, _> = expr[..pos].parse();
	num.map_or(Err("unable to parse numeric value".to_string()), |x| {
		Ok((x, &expr[pos..]))
	})
}
