use crate::{
	extract_uint, extract_uint_with_aliases, parse_cron_expression, process_list, process_list_elem,
	ListElem, ListProcessOptions, DAYS, NO_EXTRA_OPTS,
};

//---------------------------------------------------------------------

#[test]
fn extract_uint_empty_str_is_err() {
	let res = extract_uint("");
	assert!(res.is_err());
}

#[test]
fn extract_uint_str_not_starting_with_digit_is_err() {
	let res = extract_uint("$1234567890");
	assert!(res.is_err());
}

#[test]
fn extract_uint_str_with_out_of_bounds_is_err() {
	let res = extract_uint("999999999999999999999999999999999999999");
	assert!(res.is_err());
}

#[test]
fn extract_uint_str_with_valid_num_is_ok() {
	let res = extract_uint("100");
	assert!(res.is_ok());
	let (num, rest) = res.unwrap();
	assert_eq!(num, 100);
	assert_eq!(rest, "");
}

#[test]
fn extract_uint_str_with_valid_num_with_follow_up_is_ok() {
	let res = extract_uint("999999sdkjfndnfkdj0987654321");
	assert!(res.is_ok());
	let (num, rest) = res.unwrap();
	assert_eq!(num, 999999);
	assert_eq!(rest, "sdkjfndnfkdj0987654321");
}

//---------------------------------------------------------------------

const ALIASES: [[char; 3]; 5] = [['A'; 3], ['B'; 3], ['C'; 3], ['Q'; 3], ['Z'; 3]];

#[test]
fn extract_uint_with_aliases_matching() {
	let res = extract_uint_with_aliases("AAA", &ALIASES);
	assert!(res.is_ok());
	let (num, rest) = res.unwrap();
	assert_eq!(num, 0);
	assert_eq!(rest, "");
}

#[test]
fn extract_uint_with_aliases_not_matching_case() {
	let res = extract_uint_with_aliases("qQq", &ALIASES);
	assert!(res.is_ok());
	let (num, rest) = res.unwrap();
	assert_eq!(num, 3);
	assert_eq!(rest, "");
}

#[test]
fn extract_uint_with_aliases_with_numeric() {
	let res = extract_uint_with_aliases("3", &ALIASES);
	assert!(res.is_ok());
	let (num, rest) = res.unwrap();
	assert_eq!(num, 3);
	assert_eq!(rest, "");
}

//---------------------------------------------------------------------

#[test]
fn process_list_elem_empty() {
	let res = process_list_elem("", &extract_uint);
	assert!(res.is_err());
}

#[test]
fn process_list_elem_question_mark() {
	let res = process_list_elem("?123123", &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, ListElem::EmptyList);
	assert_eq!(rest, "123123");
}

#[test]
fn process_list_elem_l() {
	let res = process_list_elem("L,123", &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, ListElem::LastDay);
	assert_eq!(rest, ",123");
}

#[test]
fn process_list_elem_lw() {
	let res = process_list_elem("LW,123", &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, ListElem::LastWeekday);
	assert_eq!(rest, ",123");
}

#[test]
fn process_list_elem_last_nth() {
	let res = process_list_elem("8L,123", &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, ListElem::LastNth(8));
	assert_eq!(rest, ",123");
}

#[test]
fn process_list_elem_nearest_weekday() {
	let res = process_list_elem("13W,123", &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, ListElem::NearestWeekday(13));
	assert_eq!(rest, ",123");
}

#[test]
fn process_list_elem_nth_day_of_week_of_month() {
	let res = process_list_elem("13#321,123", &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, ListElem::NthDayOfMth(13, 321));
	assert_eq!(rest, ",123");
}

#[test]
fn process_list_elem_simple_num() {
	let res = process_list_elem("10", &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, ListElem::Num(10));
	assert_eq!(rest, "");
}

#[test]
fn process_list_elem_simple_num_with_trailing_comma() {
	let res = process_list_elem("10,", &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, ListElem::Num(10));
	assert_eq!(rest, ",");
}

#[test]
fn process_list_elem_simple_num_with_trailing_whitespace() {
	let res = process_list_elem("10     ", &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, ListElem::Num(10));
	assert_eq!(rest, "     ");
}

#[test]
fn process_list_elem_range_without_step_and_trailing_whitespace() {
	let res = process_list_elem("10-12     ", &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, ListElem::Range(10..13, 1));
	assert_eq!(rest, "     ");
}

#[test]
fn process_list_elem_range_with_step_and_trailing_whitespace() {
	let res = process_list_elem("10-12/2     ", &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, ListElem::Range(10..13, 2));
	assert_eq!(rest, "     ");
}

#[test]
fn process_list_elem_half_defined_range() {
	let res = process_list_elem("1-", &extract_uint);
	assert!(res.is_err());
}

#[test]
fn process_list_elem_custom_range_with_asterisk() {
	let res = process_list_elem("1-*/8", &extract_uint);
	assert!(res.is_err());
}

#[test]
fn process_list_elem_unrestricted_range() {
	let res = process_list_elem("*", &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, ListElem::FullRange(1));
	assert_eq!(rest, "");
}

#[test]
fn process_list_elem_unrestricted_range_with_step() {
	let res = process_list_elem("*/12", &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, ListElem::FullRange(12));
	assert_eq!(rest, "");
}

#[test]
fn process_list_elem_custom_range_with_ending_comma() {
	let res = process_list_elem("2-17,", &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, ListElem::Range(2..18, 1));
	assert_eq!(rest, ",");
}

#[test]
fn process_list_elem_unrestricted_range_with_ending_comma() {
	let res = process_list_elem("*,", &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, ListElem::FullRange(1));
	assert_eq!(rest, ",");
}

#[test]
fn process_list_elem_custom_range_with_step_with_ending_comma() {
	let res = process_list_elem("2-17/2,", &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, ListElem::Range(2..18, 2));
	assert_eq!(rest, ",");
}

#[test]
fn process_list_elem_unrestricted_range_with_step_with_ending_comma() {
	let res = process_list_elem("*/1,", &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, ListElem::FullRange(1));
	assert_eq!(rest, ",");
}

#[test]
fn process_list_elem_custom_range_with_zero_step() {
	let res = process_list_elem("2-17/0, qwerty", &extract_uint);
	assert!(res.is_err());
}

//---------------------------------------------------------------------

#[test]
fn process_list_empty() {
	let res = process_list::<0, 11, _>("", NO_EXTRA_OPTS, &extract_uint);
	assert!(res.is_err());
}

#[test]
fn process_list_trailing_comma() {
	let res = process_list::<0, 11, _>("0,1,2,3,", NO_EXTRA_OPTS, &extract_uint);
	assert!(res.is_err());
}

#[test]
fn process_list_simple_list() {
	let res = process_list::<0, 11, _>(
		"1,2,2,2,5,6,7,4,0,9,10       qwerty",
		NO_EXTRA_OPTS,
		&extract_uint,
	);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, "0 1 2 4 5 6 7 9 10");
	assert_eq!(rest, "qwerty");
}

#[test]
fn process_list_with_ranges() {
	let res = process_list::<0, 11, _>("5,0-3,7,2-4,5,7-7", NO_EXTRA_OPTS, &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, "0 1 2 3 4 5 7");
	assert_eq!(rest, "");
}

#[test]
fn process_list_with_stepped_ranges() {
	let res = process_list::<0, 11, _>("0,0-7/3,3,6-10/2", NO_EXTRA_OPTS, &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, "0 3 6 8 10");
	assert_eq!(rest, "");
}

#[test]
fn process_list_with_unrestricted_stepped_ranges_no_aliases() {
	let res = process_list::<0, 11, _>("*/4,*/2,1,3,5,7,9,*,5-10/2", NO_EXTRA_OPTS, &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, "0 1 2 3 4 5 6 7 8 9 10");
	assert_eq!(rest, "");
}

#[test]
fn process_list_with_unrestricted_stepped_ranges_with_aliases() {
	let res = process_list::<0, 7, _>("mon-6/2,Sun,2-SAT,*,1", NO_EXTRA_OPTS, &|x| {
		extract_uint_with_aliases(x, DAYS)
	});
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, "0 1 2 3 4 5 6");
	assert_eq!(rest, "");
}

#[test]
fn process_list_with_gte_upper_bound() {
	let res = process_list::<0, 11, _>("11", NO_EXTRA_OPTS, &extract_uint);
	assert!(res.is_err());
}

#[test]
fn process_list_with_lt_lower_bound() {
	let res = process_list::<6, 11, _>("5", NO_EXTRA_OPTS, &extract_uint);
	assert!(res.is_err());
}

//---------------------------------------------------------------------

fn make_allow_empty_opts(allow: bool) -> ListProcessOptions {
	ListProcessOptions {
		allow_empty: allow,
		allow_last_day: false,
		allow_last_weekday: false,
		allow_last_nth: false,
		allow_nearest_weekday: false,
		allow_nth_weekday_of_mth: false,
		has_stable_upper_bound: false,
	}
}

#[test]
fn process_list_with_not_allow_empty_and_is_empty() {
	let res = process_list::<6, 11, _>("?", make_allow_empty_opts(false), &extract_uint);
	assert!(res.is_err());
}

#[test]
fn process_list_with_allow_empty_and_is_empty() {
	let res = process_list::<6, 11, _>("?", make_allow_empty_opts(true), &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, "?");
	assert_eq!(rest, "");
}

#[test]
fn process_list_with_allow_empty_and_is_empty_after_1st_elem() {
	let res = process_list::<6, 11, _>("1,?", make_allow_empty_opts(true), &extract_uint);
	assert!(res.is_err());
}

#[test]
fn process_list_with_allow_empty_and_is_empty_with_trailing() {
	let res = process_list::<6, 11, _>("?,", make_allow_empty_opts(true), &extract_uint);
	assert!(res.is_err());
}

//---------------------------------------------------------------------

fn make_last_day_opts(allow: bool, is_stable: bool) -> ListProcessOptions {
	ListProcessOptions {
		allow_empty: false,
		allow_last_day: allow,
		allow_last_weekday: false,
		allow_last_nth: false,
		allow_nearest_weekday: false,
		allow_nth_weekday_of_mth: false,
		has_stable_upper_bound: is_stable,
	}
}

#[test]
fn process_list_with_dont_allow_last_has_last() {
	let res = process_list::<6, 11, _>("L", make_last_day_opts(false, true), &extract_uint);
	assert!(res.is_err());
}

#[test]
fn process_list_with_allow_last_has_last_and_unstable() {
	let res = process_list::<6, 11, _>("L", make_last_day_opts(true, false), &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, "L");
	assert_eq!(rest, "");
}

#[test]
fn process_list_with_allow_last_has_last_and_stable() {
	let res = process_list::<0, 11, _>(
		"1,L,3,2 qwerty",
		make_last_day_opts(true, true),
		&extract_uint,
	);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, "1 2 3 10");
	assert_eq!(rest, "qwerty");
}

//---------------------------------------------------------------------

fn make_last_weekday_opts(allow: bool) -> ListProcessOptions {
	ListProcessOptions {
		allow_empty: false,
		allow_last_day: false,
		allow_last_weekday: allow,
		allow_last_nth: false,
		allow_nearest_weekday: false,
		allow_nth_weekday_of_mth: false,
		has_stable_upper_bound: false,
	}
}

#[test]
fn process_list_with_dont_allow_last_weekday_has_last_weekday() {
	let res = process_list::<6, 11, _>("LW", make_last_weekday_opts(false), &extract_uint);
	assert!(res.is_err());
}

#[test]
fn process_list_with_allow_last_weekday_has_last_weekday() {
	let res = process_list::<0, 11, _>("LW,3,6,2-3", make_last_weekday_opts(true), &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, "2 3 6 LW");
	assert_eq!(rest, "");
}

//---------------------------------------------------------------------

fn make_last_nth_opts(allow: bool) -> ListProcessOptions {
	ListProcessOptions {
		allow_empty: false,
		allow_last_day: false,
		allow_last_weekday: false,
		allow_last_nth: allow,
		allow_nearest_weekday: false,
		allow_nth_weekday_of_mth: false,
		has_stable_upper_bound: false,
	}
}

#[test]
fn process_list_with_dont_allow_last_nth_has_last_nth() {
	let res = process_list::<0, 11, _>("3,1,2L", make_last_nth_opts(false), &extract_uint);
	assert!(res.is_err());
}

#[test]
fn process_list_with_allow_last_nth_has_last_nth() {
	let res = process_list::<0, 11, _>("3,1,2L", make_last_nth_opts(true), &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, "1 3 2L");
	assert_eq!(rest, "");
}

//---------------------------------------------------------------------

fn make_nearest_weekday_opts(allow: bool) -> ListProcessOptions {
	ListProcessOptions {
		allow_empty: false,
		allow_last_day: false,
		allow_last_weekday: false,
		allow_last_nth: false,
		allow_nearest_weekday: allow,
		allow_nth_weekday_of_mth: false,
		has_stable_upper_bound: false,
	}
}

#[test]
fn process_list_with_dont_allow_nearest_weekday_has_nearest_weekday() {
	let res = process_list::<0, 11, _>("3,1,2W", make_nearest_weekday_opts(false), &extract_uint);
	assert!(res.is_err());
}

#[test]
fn process_list_with_allow_nearest_weekday_has_nearest_weekday() {
	let res = process_list::<0, 11, _>("3,1,2W", make_nearest_weekday_opts(true), &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, "1 3 2W");
	assert_eq!(rest, "");
}

//---------------------------------------------------------------------

fn make_nth_weekday_of_mth_opts(allow: bool) -> ListProcessOptions {
	ListProcessOptions {
		allow_empty: false,
		allow_last_day: false,
		allow_last_weekday: false,
		allow_last_nth: false,
		allow_nearest_weekday: false,
		allow_nth_weekday_of_mth: allow,
		has_stable_upper_bound: false,
	}
}

#[test]
fn process_list_with_dont_allow_nth_weekday_of_mth_has_nth_weekday_of_mth() {
	let res = process_list::<0, 11, _>(
		"3,1,2#5",
		make_nth_weekday_of_mth_opts(false),
		&extract_uint,
	);
	assert!(res.is_err());
}

#[test]
fn process_list_with_allow_nth_weekday_of_mth_has_nth_weekday_of_mth() {
	let res = process_list::<0, 11, _>(
		"3,1,1#3,2#2",
		make_nth_weekday_of_mth_opts(true),
		&extract_uint,
	);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, "1 3 2#2 1#3");
	assert_eq!(rest, "");
}

#[test]
fn process_list_with_inverted_range() {
	let res = process_list::<0, 11, _>("3-1", NO_EXTRA_OPTS, &extract_uint);
	assert!(res.is_ok());
	let (expr, rest) = res.unwrap();
	assert_eq!(expr, "0 1 3 4 5 6 7 8 9 10");
	assert_eq!(rest, "");
}

#[test]
fn process_list_with_allow_nth_weekday_of_mth_has_nth_weekday_of_mth_zero_week() {
	let res = process_list::<0, 11, _>("3,1,1#0", make_nth_weekday_of_mth_opts(true), &extract_uint);
	assert!(res.is_err());
}

#[test]
fn process_list_with_allow_nth_weekday_of_mth_has_nth_weekday_of_mth_more_than_5th_week() {
	let res = process_list::<0, 11, _>("3,1,1#6", make_nth_weekday_of_mth_opts(true), &extract_uint);
	assert!(res.is_err());
}

#[test]
fn process_cron_expr_with_more_than_six_args() {
	let res = parse_cron_expression("*/15 0 1,15 * 1-5 /usr/bin/find -v foo");
	assert!(res.is_ok());
	let res = res.unwrap();
	assert_eq!(res[5], "/usr/bin/find -v foo");
}

#[test]
fn process_cron_expr_with_more_six_args() {
	let res = parse_cron_expression("*/15 0 1,15 * 1-5 /usr/bin/find");
	assert!(res.is_ok());
	let res = res.unwrap();
	assert_eq!(res[5], "/usr/bin/find");
}
