use cron_parser::{parse_cron_expression, print_output};
use std::env;

fn main() -> Result<(), String> {
	let args: Vec<String> = env::args().collect();
	if args.len() < 2 {
		return Err("no cmd line args provided".to_string());
	}
	if args.len() > 2 {
		return Err("too many cmd line args provided".to_string());
	}
	let output = parse_cron_expression(&args[1])?;
	print_output(output);
	Ok(())
}
